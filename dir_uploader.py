import glob

class DirectoryUploader(object):
    """A DirectoryUploader lists all the files in a directory"""
    def __init__(self, dirName):
        self.directoryName = dirName

    def getFiles(self):
        files = glob.glob(self.directoryName)
        return files

