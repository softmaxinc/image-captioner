import os
import re
import time
import sys
import thread
import json
import numpy as np

# Image analytics/captioning stuff
from image_data.image_caption import ImageCaption
import time

from image_data.flickr_wrapper import FlickrWrapper
from image_data.caption_generator import CaptionGenerator
from image_data.image_caption import ImageCaption
from image_data.softmax_wrapper import SoftmaxWrapper

# Load captioner
modelPath = "Z:/Desktop/CSE/NLP/model_checkpoint_flickr8k_dimirtyg-lap_baseline_17.08.p"
captionGenerator = CaptionGenerator(modelPath)

# Load image feature extractor
wrapper = SoftmaxWrapper()
flickrWrapper = FlickrWrapper()

# flickrImages = flickrWrapper.listDataset()
# flickrImageUrls = [name.url for name in flickrImages]
flickrImageUrls = ["https://softmaxstorage.blob.core.windows.net/flickr8k/1000268201_693b08cb0e.jpg"]
imageAnnotations = []
numImages = len(flickrImages)

for startIndex in range(0, numImages, 1):
    # Set image urls to classify
    
    imageUrls = flickrImageUrls[startIndex]
    features = wrapper.getImageFeatures(imageUrls)
    imageCaptions = captionGenerator.generateCaption(features)
    print("Done with {0} {1}".format(startIndex, endIndex))

# Save the data
fp = open(saveFile)
json.dump(imageAnnotations, fp)
fp.close()

