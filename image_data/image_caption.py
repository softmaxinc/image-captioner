class ImageCaption(object):

    """An ImageCaption stores image/annotation captions"""
    def __init__(self, imagePath, imageName, features, classes, labels):
        self.imagePath = imagePath
        self.imageName = imageName
        self.features = features
        self.classes = classes
        self.labels = labels
        self.captions = []

    def addCaption(self, caption):
        """ Adds a caption to the list of captions for this image"""
        self.captions.append(caption)

    def __str__(self):
        return "Image Name:{0} Path:{1} Num_Features{2}".format(self.imageName, self.imagePath, len(self.features))

