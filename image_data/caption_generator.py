import argparse
import json
import time
import datetime
import numpy as np
import code
import os
import cPickle as pickle
import math
import scipy.io

from imagernn.solver import Solver
from imagernn.imagernn_utils import decodeGenerator, eval_split

class CaptionGenerator(object):
    """ A CaptionGenerator takes as input an image url and 
       returns a sentence description for that image
    """
    def __init__(self, modelPath):
        """ Creates a new instance of a captiongenerator 
        with specified model path"""
        self.modelPath = modelPath
        self.loadModel(modelPath)

          # iterate over all images and predict sentences
        self.BatchGenerator = decodeGenerator(self.checkpoint_params)

    def loadModel(self, modelPath):
        """ Loads a model from specified path """
        # load the model
        print 'loading model %s' % (modelPath, )
        checkpoint = pickle.load(open(modelPath, 'rb'))
        self.checkpoint_params = checkpoint['params']
        self.dataset = self.checkpoint_params['dataset']
        self.model = checkpoint['model']
        self.misc = {}
        self.misc['wordtoix'] = checkpoint['wordtoix']
        self.ixtoword = checkpoint['ixtoword']

    def generateCaption(self, imageFeatures, beam_size = 30):
        img = {}
        img['feat'] = imageFeatures

        # perform the work. heavy lifting happens inside
        kwparams = { 'beam_size' : beam_size }
        Ys = self.BatchGenerator.predict([{'image':img}], self.model, self.checkpoint_params, **kwparams)

        # build up the output
        img_blob = {}

        # encode the top prediction
        top_predictions = Ys[0] # take predictions for the first (and only) image we passed in
        top_prediction = top_predictions[0] # these are sorted with highest on top
        candidate = ' '.join([self.ixtoword[ix] for ix in top_prediction[1] if ix > 0]) # ix 0 is the END token, skip that
        print 'PRED: (%f) %s' % (top_prediction[0], candidate)
        img_blob['candidate'] = {'text': candidate, 'logprob': top_prediction[0]}    
        return img_blob


