from image_data.image_caption import ImageCaption
import json
import urllib2

class SoftmaxWrapper(object):
    """SoftmaxWrapper abstracts SoftmaxVision's REST API 
    to get deep image features"""

    def __init__(self):
        self.extractionPath = "http://softmax.elasticbeanstalk.com/api/extract"
        self.classificationPath = "http://softmax.elasticbeanstalk.com/api/classify"

    def getImageFeatures(self, imageUrls):
        """ Returns image features from specified imageUrl """

        # Get raw json data
        extractionData = self.getJsonData(self.extractionPath, imageUrls)
        classificationData = self.getJsonData(self.classificationPath, imageUrls)

        # Parse it into readable class
        imageAnnotations = self.getImageInfo(extractionData, classificationData)
        return imageAnnotations

    def getJsonData(self, endpoint, imageUrls):
        """Extracts features from specified image urls"""
        # Create url string
        urlString = self.createUrlString(imageUrls)

        # Make a request to base endpoint
        req = urllib2.Request(endpoint)
        req.add_header('Content-Type', 'application/json')

        # Attach POST data
        jsonData = json.dumps({'imageurls' : urlString})

        # Wait for response
        response = urllib2.urlopen(req, jsonData)
        data = json.load(response)

        # Return json data
        return data

    def createUrlString(self, strings):
        # Creates a CSV string from an array of urls
        urlString = str.join(',', strings)
        return urlString

    def getImageInfo(self, extractedData, classificationData):
        imageCaptions = []
        """ Converts softmax's JSON data to image caption class """
        # Go through all of the batches
        currFeatureData = extractedData['Result']
        currClassificationData = extractedData['Result']

        for j in range(0, len(currFeatureData)):
            # Read in all of the data
            currImage = currFeatureData[j][0]
            currClasses = currClassificationData[j][0]

            currFeatures = currImage['Features']
            currUrl = currImage['ImageUrl']

            imageName, imageExt = self.getFileInfo(currUrl)

            # Get image details
            imageCaption = ImageCaption(currUrl, imageName + imageExt, currFeatures)
            imageCaptions.append(imageCaption)

        return imageCaptions

    def getFileInfo(self, url):
        from urlparse import urlparse
        from os.path import splitext, basename

        disassembled = urlparse(url)
        filename, file_ext = splitext(basename(disassembled.path))
        return filename, file_ext