from azure_wrapper import AzureWrapper
from dir_uploader import DirectoryUploader
from os.path import basename
import urllib2
import json
import mimetypes

class FlickrWrapper(object):
    def __init__(self, flickrDirectory = "flickr8k"):
        self.cloudWrapper = AzureWrapper()
        self.flickrDirectory = flickrDirectory

    def uploadDataset(self, directoryPath):
        # Get uploader
        uploader = DirectoryUploader("{0}/*".format(directoryPath))
        files = uploader.getFiles()

        # For each file in the files, upload it to the cloud
        for filePath in files:
            print(filePath)
            # Create the filename
            # now you can call it directly with basename
            fileName = basename(filePath)
    
            # Get file type
            fileType = mimetypes.guess_type(fileName)[0]
            print("Uploading " + fileName + " of type: " + fileType)

            # Upload it to azure
            self.cloudWrapper.upload_blob(filePath, self.flickrDirectory, fileName, fileType) 

    def listDataset(self):
        """Lists all the files in the dataset as found
        in blob storage"""
        # Get uploader
        blobs = self.cloudWrapper.list_blobs(self.flickrDirectory)
        return blobs

    def extractFeatures(self, urls):
        """Extracts features from specified image urls"""
        urlString = self.createUrlString(urls)

        req = urllib2.Request('http://softmax.elasticbeanstalk.com/api/extract')
        req.add_header('Content-Type', 'application/json')

        jsonData = json.dumps({'imageurls' : urlString})

        response = urllib2.urlopen(req, jsonData)
        data = json.load(response)
        return data

    def createUrlString(self, urls):
        """Creates a url string for getting api"""
        """Param urls: List of image urls"""
        outString = ",".join(urls)
        return outString
